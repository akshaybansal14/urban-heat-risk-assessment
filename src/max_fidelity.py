import cvxpy as cp
import numpy as np

class max_fidelity:

    # Constructor to initialize object
    def __init__(self, **kwargs):
        self.states = kwargs['qstates'] # input quantum states (density matrices)
        self.num_states = len(self.states) # total number of quantum states
        self.dimension_state = self.states[0].shape[0] # dimension of the quantum state

    # Create density variable for central fidelity of the input quantum states 
    def __create_opt_var(self):
        rho = cp.Variable((self.dimension_state, self.dimension_state), PSD = True)
        list_X = []
        for i in range(self.num_states):
            list_X.append(cp.Variable((2 * self.dimension_state, 2 * self.dimension_state), PSD = True))
        return rho, list_X
        
    # Add constraints for the SDP
    def __add_constraints(self, rho, list_X):
        constr = []
        for i in range(self.num_states):
            constr += [list_X[i][0:self.dimension_state, 0:self.dimension_state] == rho, list_X[i][self.dimension_state:2 * self.dimension_state, self.dimension_state:2 * self.dimension_state] == self.states[i]]
            constr += [list_X[i] == list_X[i].T]
        constr += [rho == rho.T, cp.trace(rho) == 1]
        return constr

    # Return optimal state with maximum fidelity with other states
    def get_optimal_state(self):
        rho, list_X = self.__create_opt_var()
        constr = self.__add_constraints(rho, list_X)
        C = (1/2) * np.block([[np.zeros([self.dimension_state,self.dimension_state]), np.identity(self.dimension_state)], [np.identity(self.dimension_state), np.zeros([self.dimension_state,self.dimension_state])]])
        obj = 0
        for i in range(self.num_states):
            obj += cp.trace(C @ list_X[i])
        prob = cp.Problem(cp.Maximize(obj), constr)
        prob.solve(verbose = True)
        print(prob.status)
        print(obj.value)

        return rho.value