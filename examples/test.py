from max_fidelity import max_fidelity
import numpy as np
from numpy import sqrt
import pandas as pd
import sys

def main():

    
    ket1 = np.array([1,0]) 
    ket2 = (1/sqrt(2)) * np.array([1,1])
    ket3 = np.array([0,1])

    list_states = [np.outer(ket1,ket1), np.outer(ket2,ket2), np.outer(ket3,ket3)]
    print(max_fidelity(qstates = list_states).get_optimal_state())

    return

if __name__ == '__main__':
    main()